package smf_service

import (
	"fmt"
	"free5gc/lib/http2_util"
	"free5gc/lib/openapi/models"
	"free5gc/lib/path_util"
	"free5gc/lib/pfcp/pfcpUdp"
	"net"
	"time"

	"bitbucket.org/sothy5/smf-go/EventExposure"
	"bitbucket.org/sothy5/smf-go/PDUSession"
	"bitbucket.org/sothy5/smf-go/factory"
	"bitbucket.org/sothy5/smf-go/logger"
	"bitbucket.org/sothy5/smf-go/smf_consumer"
	"bitbucket.org/sothy5/smf-go/smf_context"
	"bitbucket.org/sothy5/smf-go/smf_handler"
	"bitbucket.org/sothy5/smf-go/smf_pfcp/pfcp_message"
	"bitbucket.org/sothy5/smf-go/smf_pfcp/pfcp_udp"
	"bitbucket.org/sothy5/smf-go/smf_util"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

type SMF struct{}

var initLog *log.Entry

func init() {
	initLog = logger.InitLog
}

func (*SMF) Initialize(c *cli.Context) {

	smfcfg := c.String("smfcfg")

	if smfcfg != "" {
		factory.InitConfigFactory(smfcfg)
	} else {
		DefaultSmfConfigPath := path_util.Gofree5gcPath("free5gc/config/smfcfg.conf")
		factory.InitConfigFactory(DefaultSmfConfigPath)
	}

	logger.SetLogLevel(log.WarnLevel)
	logger.SetReportCaller(true)
}

func (smf *SMF) Start() {
	smf_context.InitSmfContext(&factory.SmfConfig)

	initLog.Infoln("Server started")
	router := gin.Default()

	err := smf_consumer.SendNFRegistration()

	if err != nil {
		retry_err := smf_consumer.RetrySendNFRegistration(10)
		if retry_err != nil {
			logger.InitLog.Errorln(retry_err)
			return
		}
	}

	for _, serviceName := range factory.SmfConfig.Configuration.ServiceNameList {
		switch models.ServiceName(serviceName) {
		case models.ServiceName_NSMF_PDUSESSION:
			PDUSession.AddService(router)
		case models.ServiceName_NSMF_EVENT_EXPOSURE:
			EventExposure.AddService(router)
		}
	}
	pfcp_udp.Run()

	for _, upf := range factory.SmfConfig.Configuration.UPF {
		if upf.Port == 0 {
			upf.Port = pfcpUdp.PFCP_PORT
		}
		addr, err := net.ResolveUDPAddr("udp", fmt.Sprintf("%s:%d", upf.Addr, upf.Port))
		if err != nil {
			logger.InitLog.Warnln("UPF addr error")
		}
		pfcp_message.SendPfcpAssociationSetupRequest(addr)
		logger.InitLog.Infoln("What happened to PFCP")
	}

	time.Sleep(1000 * time.Millisecond)

	go smf_handler.Handle()
	HTTPAddr := fmt.Sprintf("%s:%d", smf_context.SMF_Self().HTTPAddress, smf_context.SMF_Self().HTTPPort)
	server, _ := http2_util.NewServer(HTTPAddr, smf_util.SmfLogPath, router)

	initLog.Infoln(server.ListenAndServeTLS(smf_util.SmfPemPath, smf_util.SmfKeyPath))
}

/*
func (smf *SMF) Exec(c *cli.Context) error {
	initLog.Traceln("args:", c.String("smfcfg"))
	args := smf.FilterCli(c)
	initLog.Traceln("filter: ", args)
	command := exec.Command("./smf", args...)

	stdout, err := command.StdoutPipe()
	if err != nil {
		initLog.Fatalln(err)
	}
	wg := sync.WaitGroup{}
	wg.Add(3)
	go func() {
		in := bufio.NewScanner(stdout)
		for in.Scan() {
			fmt.Println(in.Text())
		}
		wg.Done()
	}()

	stderr, err := command.StderrPipe()
	if err != nil {
		initLog.Fatalln(err)
	}
	go func() {
		in := bufio.NewScanner(stderr)
		for in.Scan() {
			fmt.Println(in.Text())
		}
		wg.Done()
	}()

	go func() {
		if err := command.Start(); err != nil {
			initLog.Errorf("SMF Start error: %v", err)
		}
		wg.Done()
	}()

	wg.Wait()

	return err
}
*/
